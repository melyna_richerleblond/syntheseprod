//
//  LieuxTVC.swift
//  syntheseProd
//
//  Created by Etudiant on 16-12-01.
//  Copyright © 2016 tim. All rights reserved.
//

import UIKit

class LieuxTVC: UITableViewController {

    
    private var dic_resultats  = Dictionary<String, Any>()
    private var _listeDesEndroits = Array<Dictionary<String, Any>>()
    
    // URL vers l'API google places
    //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=5000&types=food&key=AIzaSyCUPWpMnnILWs-9I8GAkYFkn1ZXHcXZH6Y
    
    let urlPartie1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="
    var location  = "45.7777418,-73.9980053"
    let urlPartie2 = "&radius="
    var radius = "5000"
    var radiusTest="5"
    let urlPartie3 = "&types="
    var placeType = "food"
    let urlPartie4 = "&key="
    let key = "AIzaSyCUPWpMnnILWs-9I8GAkYFkn1ZXHcXZH6Y"
    var googlePlaceAPI = ""
    
    func obtenirLesDonnées(_ url:String) {
        let uneURL = URL(string: url)!  //Danger!
        
        /// Exécuter le traitement  suivant en parallèle
        /// DispatchQueue.main.async ( execute: {
        if let _données = NSData(contentsOf: uneURL) as? Data {
            do {
                let json = try JSONSerialization.jsonObject(with: _données, options: JSONSerialization.ReadingOptions()) as? Dictionary<String, /*Dictionary<String,*/ Any>
                
                print("Conversion JSON réussie")
                self.dic_resultats = json!
                //  print(self.dic_resultats)
                // Créer un tableau à partir du champ 'resultats'
                if let listeDesEndroits = (self.dic_resultats["results"]as? Array<Dictionary<String, Any>>) {
                    self._listeDesEndroits = listeDesEndroits
                    //print("Liste des Endroits:\n\(self._listeDesEndroits)")
                    // self.collectionDesItems.reloadData()
                }
                // print(json)
            } catch {
                print("\n\n#Erreur: Problème de conversion json:\(error)\n\n")
            } // do/try/catch
        } else
        {
            print("\n\n#Erreur: impossible de lire les données via:\(self.googlePlaceAPI)\n\n")
        } // if let _données = NSData
        /// }) // DispatchQueue.main.async
        afficherEndroits()
    } // obtenirLesDonnées
    
    
    func afficherEndroits() {
        print(_listeDesEndroits.count)
        print("---------------------------------------")
        for endroit in _listeDesEndroits{
            if let _nom = endroit["name"], let _cote = endroit["rating"], let _adresse = endroit["vicinity"] {
                print("***Action: \(_nom), cote: \(_cote), adresse : \(_adresse)")
            }
        } // for action in
        print("---------------------------------------")
        print(_listeDesEndroits.count)
    } // afficherActions()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        googlePlaceAPI = urlPartie1 + location + urlPartie2 + radius + urlPartie3 + placeType + urlPartie4 + key
        obtenirLesDonnées(googlePlaceAPI)
        print(googlePlaceAPI)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    
    
   override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows

        return _listeDesEndroits.count
        
    }

    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
