//
//  intro.swift
//  syntheseProd
//
//  Created by Etudiant on 16-11-24.
//  Copyright © 2016 tim. All rights reserved.
//

import UIKit

class intro: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
         Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(intro.passerAuMenuPrincipal), userInfo: nil, repeats: false)
        // Do any additional setup after loading the view.
    }


    func passerAuMenuPrincipal(){
        performSegue(withIdentifier: "versMenuPrincipal", sender: self)
    } // passerAuMenuPrincipal
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
