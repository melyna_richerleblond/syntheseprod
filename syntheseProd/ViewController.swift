//
//  ViewController.swift
//  syntheseProd
//
//  Created by Etudiant on 16-11-17.
//  Copyright © 2016 tim. All rights reserved.
//
//
//Key :         

import UIKit

struct criteres {
    var location = ""
    var radius   = ""
    var placeType = ""
}

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{

    var typeRecherche  = "" 
    var placeType = ["airport","insurance_agency","real_estate_agency","travel_agency","pet_store","aquarium","lawyer","bank","bar","library","jewelry_store","bowling_alley","laundry","post_office","local_government_office","cafe","campground","fire_station","casino","shopping_mall","cemetery","movie_theater","moving_company","car_dealer","dentist","convenience_store","night_club","doctor","school","church","electrician","storage","grocery_or_supermarket","finance","florist","art_gallery","car_repair","bus_station","train_station","atm","gym","lodging","hospital","city_hall","car_wash","book_store","movie_rental","car_rental","store","department_store","electronics_store","furniture_store","shoe_store","clothing_store","liquor_store","funeral_home","mosque","museum","food","courthouse","park","amusement_park","bakery","painter","pharmacy","physiotherapist","plumber","police","hardware_store","restaurant","health","beauty_salon","locksmith","spa","stadium","gas_station","subway_station","parking","synagogue","university","veterinary_care","zoo"]
    var data = criteres()
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var lieuxRechercher: UITextField!
    @IBOutlet weak var rayonDemander: UITextField!
    @IBAction func lancerRecherche(_ sender: AnyObject) {
         data = criteres.init(location: "\(lieuxRechercher.text)", radius: "\(rayonDemander.text)", placeType: "\(typeRecherche)")
        print("#\(data)")
    }
    
   
    
    var pickerData: [String] = [String]()
    
     override func viewDidLoad() {
        
        // Connect data:
        self.picker.delegate = self
        self.picker.dataSource = self
        pickerData = ["Aéroport","Agence d’assurances","Agence d’immeuble","Agence de voyage","Animalerie","Aquarium","Avocat","Banque","Bar","Bibliothèque","Bijouterie","Bowling","Buanderie","Bureau de poste","Bureau gouvernemental","Café","Camping","Caserne de pompier","Casino","Centre d'achat","Cimetière","Cinéma","Compagnie de déménagement","Concessionnaire","Dentiste","Dépanneur","Discothèque","Docteur","École","Église","Électricien","Entrepôt","Épicerie","Fiance","Fleuriste","Galerie d'art","Garage","Gare d'autobus","Gare de train","Guichet ATM","Gym","Hébergement","Hôpital","Hôtel de ville","Lave-auto","Librairie","Location de films","Location de voiture","Magasin","Magasin à rayons","Magasin d'électronique","Magasin de meubles","Magasin de souliers","Magasin de vêtements","Magasin de vins et spiritueux","Maison funéraire","Mosquée","Musée","Nourriture","Palais de justice","Parc","Parc d'amusement","Pâtisserie","Peintre","Pharmacie","Physiothérapie","Plombier","Police","Quincaillerie","Restaurent","Santé","Salon de beauté","Serrurier","Spa","Satde","Station de gas","Station de métro","Stationnement","Synagogue","Université","Vétérinaire","Zoo"]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        // unVar = row
        typeRecherche = "\(placeType[row])"
        print(typeRecherche)
    }

}

